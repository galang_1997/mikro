const CoreServices = require("../services/CoreServices")
const logger = require("../logger/logger")
const toExcel = require("../helper/toExcel")

class RealisasiPikroControllers {
    static async getRealisasiPikro() {
        logger.info("[Get Data from getRealisasiPikro]");
        const realisasiPikro = await CoreServices.getRealisasiPikro()
        toExcel("REALISASI_PIKRO.xlsx", realisasiPikro)
        return realisasiPikro ? true : false
    } catch (error) {
        logger.error(
            `[Get Data from Controller getRealisasiPikro: ${error}]`
        );
    }
}

module.exports = RealisasiPikroControllers
