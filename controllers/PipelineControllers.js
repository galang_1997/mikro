const MikroServices = require("../services/MikroServices")
const logger = require("../logger/logger")
const toExcel = require("../helper/toExcel")

class PipelineControllers {
    static async getPipeline() {
        logger.info("[Get Data from getPipeline]");
        const pipeline = await MikroServices.getPipeline()
        toExcel("PIPELINE.xlsx", pipeline)
        return pipeline ? true : false
    }
}

module.exports = PipelineControllers