const CoreServices = require("../services/CoreServices")
const logger = require("../logger/logger")
const toExcel = require("../helper/toExcel")

class DpkPikroControllers {
    static async getDpkPikro() {
        logger.info("[Get Data from getDpkPikro]");
        const realisasiPikro = await CoreServices.getDpkPikro()
        toExcel("DPK_PIKRO.xlsx", realisasiPikro)
        return realisasiPikro ? true : false
    } catch (error) {
        logger.error(
            `[Get Data from Controller getDpkPikro: ${error}]`
        );
    }
}

module.exports = DpkPikroControllers
