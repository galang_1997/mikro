const MikroServices = require("../services/MikroServices")
const CoreServices = require("../services/CoreServices")
const logger = require("../logger/logger")
const { 
    kol,
    statusPrakarsa
} = require("../constanta/const")
const toRupiah = require("../helper/toRupiah")

class ReportPerformaControllers {
    static async getReaportPerforma() {
        logger.info("[Get Data from getReaportPerforma]");

        const currentDate = new Date().toISOString().split('T')[0].split('-')
        const year = currentDate[0]
        const month = currentDate[1]
        const date = currentDate[2]

        try {
            const dataPrakarsa = await MikroServices.getPrakarsa()
            const dataPrakarsaByDate = await MikroServices.getPrakarsaByDate()
            const dataKolektibilitas = await CoreServices.getKolektibilitas()
            const dataKolektibilitasByDate = await CoreServices.getKolektibilitasByDate()
            const totalNasabahAktif = await CoreServices.getNasabahAktif()
            // const totalDisburseCurrentYear = dataKolektibilitasByDate[0].PLAFOND
            let totalDisburseCurrentYear = 0

            dataKolektibilitasByDate.forEach(val => {
                totalDisburseCurrentYear += +val.PLAFOND
            })


            // set totalOutstanding, totalPembayaran, totalNasabah
            let totalOutstanding = 0
            let totalDisburse = 0
            let totalPembayaran = 0
            let totalNasabah = 0
            dataKolektibilitas.forEach((dtKolektibilitas) => {
                totalOutstanding += dtKolektibilitas.OUTSTANDING
                totalDisburse += dtKolektibilitas.PLAFOND
                totalPembayaran += dtKolektibilitas.PEMBAYARAN_POKOK
                totalPembayaran += dtKolektibilitas.PEMBAYARAN_BUNGA
                totalPembayaran += dtKolektibilitas.PEMBAYARAN_DENDA
                totalNasabah += dtKolektibilitas.NOA
            })

            // Set kolektibilitas data
            const kolektibilitas = {}
            kol.forEach((kol) => {
                let isInclude = false
                dataKolektibilitas.forEach((dtKolektibilitas) => {
                    if ((kol == dtKolektibilitas.KOL) && dtKolektibilitas.STATUS == 1) {
                        kolektibilitas[kol] = {
                            totalNoa: dtKolektibilitas.NOA,
                            totalOutstanding: dtKolektibilitas.OUTSTANDING
                        }
                        isInclude = true
                    }

                    if (!isInclude) {
                        kolektibilitas[kol] = {
                        totalNoa: 0,
                        totalOutstanding: 0
                        }
                    }
                })
            })

            // Set DOK NPL data
            const DPK = ((kolektibilitas['Kol 2'].totalOutstanding/totalOutstanding)*100).toFixed(2)
            const NPL = (((kolektibilitas['Kol 3'].totalOutstanding + kolektibilitas['Kol 4'].totalOutstanding + kolektibilitas['Kol 5'].totalOutstanding)/totalOutstanding)*100).toFixed(2)
            
            // Set prakarsa data
            const prakarsa = {}
            statusPrakarsa.forEach((stsPrakarsa) => {
                let isInclude= false
                dataPrakarsa.forEach((dtPrakarsa) => {
                    if (stsPrakarsa == dtPrakarsa.status_prakarsa) {
                        prakarsa[stsPrakarsa] = {
                            TotalPrakarsa: dtPrakarsa.Total_Prakarsa ? dtPrakarsa.Total_Prakarsa : 0,
                            nominalPinjaman: dtPrakarsa.nominal_pinjaman ? dtPrakarsa.nominal_pinjaman : 0,
                            TotalPrakarsaDaily: 0,
                            nominalPinjamanDaily: 0

                        }
                        isInclude = true
                    }

                    if (!isInclude) {
                        prakarsa[stsPrakarsa] = {
                            TotalPrakarsa: 0,
                            nominalPinjaman: 0,
                            TotalPrakarsaDaily: 0,
                            nominalPinjamanDaily: 0
                        }
                    }
                })
            })

            statusPrakarsa.forEach((stsPrakarsa) => {
                dataPrakarsaByDate.forEach((dtPrakarsaByDate) => {
                    if (stsPrakarsa == dtPrakarsaByDate.status_prakarsa) {
                        prakarsa[stsPrakarsa].TotalPrakarsaDaily = dtPrakarsaByDate.Total_Prakarsa ? dtPrakarsaByDate.Total_Prakarsa : 0
                        prakarsa[stsPrakarsa].nominalPinjamanDaily = dtPrakarsaByDate.nominal_pinjaman ? dtPrakarsaByDate.nominal_pinjaman : 0
                    }
                })
            })

            //Set message telegram
            let message = `   
SUMMARY REPORT PERFORMA PINANG MIKRO TGL ${date}-${month}-${year}
===========================================
#PRODUCT OVERVIEW
TOTAL OUTSTANDING | Rp.${toRupiah(totalOutstanding)}
TOTAL DISBURSE | Rp.${toRupiah(totalDisburse)}
TOTAL DISBURSE ${year} |Rp.${toRupiah(totalDisburseCurrentYear)}
TOTAL PEMBAYARAN |Rp.${toRupiah(totalPembayaran)} 
TOTAL NASABAH | ${totalNasabah}
TOTAL NASABAH AKTIF | ${totalNasabahAktif[0].NOA}

#KOLEKTIBILITAS
Kolektibilitas | Total Noa | Total OS
Kol 1 | ${kolektibilitas['Kol 1'].totalNoa} | Rp.${toRupiah(kolektibilitas['Kol 1'].totalOutstanding)}
Kol 2 | ${kolektibilitas['Kol 2'].totalNoa} | Rp.${toRupiah(kolektibilitas['Kol 2'].totalOutstanding)}
Kol 3 | ${kolektibilitas['Kol 3'].totalNoa} | Rp.${toRupiah(kolektibilitas['Kol 3'].totalOutstanding)}
Kol 4 | ${kolektibilitas['Kol 4'].totalNoa} | Rp.${toRupiah(kolektibilitas['Kol 4'].totalOutstanding)}
Kol 5 | ${kolektibilitas['Kol 5'].totalNoa} | Rp.${toRupiah(kolektibilitas['Kol 5'].totalOutstanding)}

#PRESENTASE DPK / NPL
%DPK | ${DPK}%
%NPL | ${NPL}%

#NOA
Status | All Time Noa | Daily Noa
ANALISA PINJAMAN DITOLAK AO | ${prakarsa['ANALISA PINJAMAN DITOLAK AO'].TotalPrakarsa} | ${prakarsa['ANALISA PINJAMAN DITOLAK AO'].TotalPrakarsaDaily}
PRESCREENING DITOLAK AO | ${prakarsa['PRESCREENING DITOLAK AO'].TotalPrakarsa} | ${prakarsa['PRESCREENING DITOLAK AO'].TotalPrakarsaDaily}
PRAKARSA DITOLAK NASABAH | ${prakarsa['PRAKARSA DITOLAK NASABAH'].TotalPrakarsa} | ${prakarsa['PRAKARSA DITOLAK NASABAH'].TotalPrakarsaDaily}
PRAKARSA DITOLAK | ${prakarsa['PRAKARSA DITOLAK'].TotalPrakarsa} | ${prakarsa['PRAKARSA DITOLAK'].TotalPrakarsaDaily}
PRESCREENING DITOLAK | ${prakarsa['PRESCREENING DITOLAK'].TotalPrakarsa} | ${prakarsa['PRESCREENING DITOLAK'].TotalPrakarsaDaily}
PROSES INPUT PRESCREENING (0) | ${prakarsa['PROSES INPUT PRESCREENING (0)'].TotalPrakarsa} | ${prakarsa['PROSES INPUT PRESCREENING (0)'].TotalPrakarsaDaily}
PROSES INPUT PRESCREENING (1) | ${prakarsa['PROSES INPUT PRESCREENING (1)'].TotalPrakarsa} | ${prakarsa['PROSES INPUT PRESCREENING (1)'].TotalPrakarsaDaily}
MENUNGGU HASIL PRESCREENING | ${prakarsa['MENUNGGU HASIL PRESCREENING'].TotalPrakarsa} | ${prakarsa['MENUNGGU HASIL PRESCREENING'].TotalPrakarsaDaily}
PRESCREENING DISETUJUI | ${prakarsa['PRESCREENING DISETUJUI'].TotalPrakarsa} | ${prakarsa['PRESCREENING DISETUJUI'].TotalPrakarsaDaily}
PROCESS INPUT CRS | ${prakarsa['PROCESS INPUT CRS'].TotalPrakarsa} | ${prakarsa['PROCESS INPUT CRS'].TotalPrakarsaDaily}
MENUNGGU PUTUSAN | ${prakarsa['MENUNGGU PUTUSAN'].TotalPrakarsa} | ${prakarsa['MENUNGGU PUTUSAN'].TotalPrakarsaDaily}
PRAKARSA DISETUJUI, MINTA PERSETUJUAN NASABAH | ${prakarsa['PRAKARSA DISETUJUI, MINTA PERSETUJUAN NASABAH'].TotalPrakarsa} | ${prakarsa['PRAKARSA DISETUJUI, MINTA PERSETUJUAN NASABAH'].TotalPrakarsaDaily}
PROSES AKAD KREDIT | ${prakarsa['PROSES AKAD KREDIT'].TotalPrakarsa} | ${prakarsa['PROSES AKAD KREDIT'].TotalPrakarsaDaily}
PROSES PENCAIRAN | ${prakarsa['PROSES PENCAIRAN'].TotalPrakarsa} | ${prakarsa['PROSES PENCAIRAN'].TotalPrakarsaDaily}
PENCAIRAN BERHASIL | ${prakarsa['PENCAIRAN BERHASIL'].TotalPrakarsa} | ${prakarsa['PENCAIRAN BERHASIL'].TotalPrakarsaDaily}

#Amount
Status | All Time Noa | Daily Noa
ANALISA PINJAMAN DITOLAK AO | Rp.${toRupiah(prakarsa['ANALISA PINJAMAN DITOLAK AO'].nominalPinjaman)} | Rp.${toRupiah(prakarsa['ANALISA PINJAMAN DITOLAK AO'].nominalPinjamanDaily)}
PRESCREENING DITOLAK AO | Rp.${toRupiah(prakarsa['PRESCREENING DITOLAK AO'].nominalPinjaman)} | Rp.${toRupiah(prakarsa['PRESCREENING DITOLAK AO'].nominalPinjamanDaily)}
PRAKARSA DITOLAK NASABAH | Rp.${toRupiah(prakarsa['PRAKARSA DITOLAK NASABAH'].nominalPinjaman)} | Rp.${toRupiah(prakarsa['PRAKARSA DITOLAK NASABAH'].nominalPinjamanDaily)}
PRAKARSA DITOLAK | Rp.${toRupiah(prakarsa['PRAKARSA DITOLAK'].nominalPinjaman)} | Rp.${toRupiah(prakarsa['PRAKARSA DITOLAK'].nominalPinjamanDaily)}
PRESCREENING DITOLAK | Rp.${toRupiah(prakarsa['PRESCREENING DITOLAK'].nominalPinjaman)} | Rp.${toRupiah(prakarsa['PRESCREENING DITOLAK'].nominalPinjamanDaily)}
PROSES INPUT PRESCREENING (0) | Rp.${toRupiah(prakarsa['PROSES INPUT PRESCREENING (0)'].nominalPinjaman)} | Rp.${toRupiah(prakarsa['PROSES INPUT PRESCREENING (0)'].nominalPinjamanDaily)}
PROSES INPUT PRESCREENING (1) | Rp.${toRupiah(prakarsa['PROSES INPUT PRESCREENING (1)'].nominalPinjaman)} | Rp.${toRupiah(prakarsa['PROSES INPUT PRESCREENING (1)'].nominalPinjamanDaily)}
MENUNGGU HASIL PRESCREENING | Rp.${toRupiah(prakarsa['MENUNGGU HASIL PRESCREENING'].nominalPinjaman)} | Rp.${toRupiah(prakarsa['MENUNGGU HASIL PRESCREENING'].nominalPinjamanDaily)}
PRESCREENING DISETUJUI | Rp.${toRupiah(prakarsa['PRESCREENING DISETUJUI'].nominalPinjaman)} | Rp.${toRupiah(prakarsa['PRESCREENING DISETUJUI'].nominalPinjamanDaily)}
PROCESS INPUT CRS | Rp.${toRupiah(prakarsa['PROCESS INPUT CRS'].nominalPinjaman)} | Rp.${toRupiah(prakarsa['PROCESS INPUT CRS'].nominalPinjamanDaily)}
MENUNGGU PUTUSAN | Rp.${toRupiah(prakarsa['MENUNGGU PUTUSAN'].nominalPinjaman)} | Rp.${toRupiah(prakarsa['MENUNGGU PUTUSAN'].nominalPinjamanDaily)}
PRAKARSA DISETUJUI, MINTA PERSETUJUAN NASABAH | Rp.${toRupiah(prakarsa['PRAKARSA DISETUJUI, MINTA PERSETUJUAN NASABAH'].nominalPinjaman)} | Rp.${toRupiah(prakarsa['PRAKARSA DISETUJUI, MINTA PERSETUJUAN NASABAH'].nominalPinjamanDaily)}
PROSES AKAD KREDIT | Rp.${toRupiah(prakarsa['PROSES AKAD KREDIT'].nominalPinjaman)} | Rp.${toRupiah(prakarsa['PROSES AKAD KREDIT'].nominalPinjamanDaily)}
PROSES PENCAIRAN | Rp.${toRupiah(prakarsa['PROSES PENCAIRAN'].nominalPinjaman)} | Rp.${toRupiah(prakarsa['PROSES PENCAIRAN'].nominalPinjamanDaily)}
PENCAIRAN BERHASIL | Rp.${toRupiah(prakarsa['PENCAIRAN BERHASIL'].nominalPinjaman)} | Rp.${toRupiah(prakarsa['PENCAIRAN BERHASIL'].nominalPinjamanDaily)}

We would be very happy to assist you.
Thank you.
Best Regards,
Pinang Mikro Ops
`
        return message
    } catch (error) {
        logger.error(
            `[Get Data from Controller getDataMikro: ${error}]`
        );
    }
  }s
}

module.exports = ReportPerformaControllers
