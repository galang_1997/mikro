const MikroServices = require("../services/MikroServices")
const logger = require("../logger/logger")
const toExcel = require("../helper/toExcel")

class AllPrakarsaControllers {
    static async getAllPrakarsa() {
        logger.info("[Get Data from getAllPrakarsa]");
        const allPrakarsa = await MikroServices.getAllPrakarsa()
        toExcel("ALL_PRAKARSA.xlsx", allPrakarsa)
        return allPrakarsa ? true : false
    }
}

module.exports = AllPrakarsaControllers