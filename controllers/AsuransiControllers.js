const MikroServices = require("../services/MikroServices")
const logger = require("../logger/logger")
const toExcel = require("../helper/toExcel")

class AsuransiControllers {
    static async getAsuransi(startDT, endDT) {
        logger.info("[Get Data from getAsuransi]");
        const asuransi = await MikroServices.getAsuransi(startDT, endDT)
        toExcel("ASURANSI.xlsx", asuransi)
        return asuransi ? true : false
    }
}

module.exports = AsuransiControllers