const MikroServices = require("../services/MikroServices")
const logger = require("../logger/logger")
const toExcel = require("../helper/toExcel")

class Status78Controllers {
    static async getStatus78() {
        logger.info("[Get Data from getStatus78]");
        const status78 = await MikroServices.getStatus78()
        toExcel("STATUS78.xlsx", status78)
        return status78 ? true : false
    }
}

module.exports = Status78Controllers