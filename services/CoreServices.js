const sql = require('mssql')
const db_core = require("../connection/config_core")
const { getKolektibilitas, getKolektibilitasByDate, getRealisasiPikro, getDpkPikro, getNasabahAktif } = require("../query/core")
const logger = require("../logger/logger")

class CoreServices {
  static async getKolektibilitas() {
    try {
      await sql.connect(db_core)
      logger.info("[Run Service getKolektibilitas || DB CORE]");
      const data = await sql.query(getKolektibilitas)
      await sql.close()
      return data.recordset
    } catch (error) {
      logger.error(
        `[Run Service getKolektibilitas: ${error}]`
      );
    }
  }

  static async getKolektibilitasByDate() {
    try {
      await sql.connect(db_core)
      logger.info("[Run Service getKolektibilitasByDate || DB CORE]");
      const data = await sql.query(getKolektibilitasByDate)
      await sql.close()
      return data.recordset
    } catch (error) {
      logger.error(
        `[Run Service getKolektibilitasByDate: ${error}]`
      );
    }
  }
  
  static async getRealisasiPikro() {
    try {
      await sql.connect(db_core)
      logger.info("[Run Service getRealisasiPikro || DB CORE]");
      const data = await sql.query(getRealisasiPikro)
      await sql.close()
      return data.recordset
    } catch (error) {
      logger.error(
        `[Run Service getRealisasiPikro: ${error}]`
      );
    }
  }
  
  static async getDpkPikro() {
    try {
      await sql.connect(db_core)
      logger.info("[Run Service getDpkPikro || DB CORE]");
      const data = await sql.query(getDpkPikro)
      await sql.close()
      return data.recordset
    } catch (error) {
      logger.error(
        `[Run Service getDpkPikro: ${error}]`
      );
    }
  }

  static async getNasabahAktif() {
    try {
      await sql.connect(db_core)
      logger.info("[Run Service getNasabahAktif || DB MIKRO]");
      const data = await sql.query(getNasabahAktif)
      await sql.close()
      return data.recordset
    } catch (error) {
      logger.error(
        `[Run Service getNasabahAktif: ${error}]`
      );
    }
  }
}

module.exports = CoreServices