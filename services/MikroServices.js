const sql = require('mssql')
const db_mikro = require("../connection/config_mikro")
const { 
  getPrakarsa, getPrakarsaByDate, getAllPrakarsa, getPipeline, getStatus78, getAsuransi } = require("../query/mikro")
const logger = require("../logger/logger")

class MikroServices {
  static async getPrakarsa() {
    try {
      await sql.connect(db_mikro)
      logger.info("[Run MikroServices getPrakarsa || DB MIKRO]");
      const data = await sql.query(getPrakarsa)
      await sql.close()
      return data.recordset
    } catch (error) {
      logger.error(
        `[Run MikroServices getDataMikro: ${error}]`
      );
    }
  }

  static async getPrakarsaByDate() {
    try {
      await sql.connect(db_mikro)
      logger.info("[Run MikroServices getPrakarsaByDate || DB MIKRO]");
      const data = await sql.query(getPrakarsaByDate)
      await sql.close()
      return data.recordset
    } catch (error) {
      logger.error(
        `[Run MikroServices getPrakarsaByDate: ${error}]`
      );
    }
  }

  static async getAllPrakarsa() {
    try {
      await sql.connect(db_mikro)
      logger.info("[Run MikroServices getAllPrakarsa || DB MIKRO]");
      const data = await sql.query(getAllPrakarsa)
      await sql.close()
      return data.recordset
    } catch (error) {
      logger.error(
        `[Run MikroServices getAllPrakarsa: ${error}]`
      );
    }
  }

  static async getPipeline() {
    try {
      await sql.connect(db_mikro)
      logger.info("[Run MikroServices getPipeline || DB MIKRO]");
      const data = await sql.query(getPipeline)
      await sql.close()
      return data.recordset
    } catch (error) {
      logger.error(
        `[Run MikroServices getPipeline: ${error}]`
      );
    }
  }

  static async getStatus78() {
    try {
      await sql.connect(db_mikro)
      logger.info("[Run MikroServices getStatus78 || DB MIKRO]");
      const data = await sql.query(getStatus78)
      await sql.close()
      return data.recordset
    } catch (error) {
      logger.error(
        `[Run MikroServices getStatus78: ${error}]`
      );
    }
  }

  static async getAsuransi(startDT, endDT) {
    try {
      await sql.connect(db_mikro)
      logger.info("[Run MikroServices getAsuransi || DB MIKRO]");
      const data = await sql.query(getAsuransi(startDT, endDT))
      await sql.close()
      return data.recordset
    } catch (error) {
      logger.error(
        `[Run MikroServices getAsuransi: ${error}]`
      );
    }
  }
}

module.exports = MikroServices