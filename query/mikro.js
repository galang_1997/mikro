const currentDate = new Date().toISOString().split('T')[0]

const getPrakarsa = `
    select
    CASE prakarsa.status
        WHEN 0 THEN 'PROSES INPUT PRESCREENING (0)'
        WHEN 1 THEN 'PROSES INPUT PRESCREENING (1)'
        WHEN 2 THEN 'MENUNGGU HASIL PRESCREENING'
        WHEN 3 THEN 'PRESCREENING DISETUJUI'
        WHEN 4 THEN 'PROCESS INPUT CRS'
        WHEN 5 THEN 'MENUNGGU PUTUSAN'
        WHEN 6 THEN 'PRAKARSA DISETUJUI, MINTA PERSETUJUAN NASABAH'
        WHEN 7 THEN 'PROSES AKAD KREDIT'
        WHEN 8 THEN 'PROSES PENCAIRAN'
        WHEN 9 THEN 'PENCAIRAN BERHASIL'
        WHEN -1 THEN 'PRESCREENING ERROR'
        WHEN -2 THEN 'PRESCREENING DITOLAK'
        WHEN -3 THEN 'PRAKARSA DITOLAK'
        WHEN -4 THEN 'PRAKARSA DITOLAK NASABAH'
        WHEN -5 THEN 'PRESCREENING DITOLAK AO'
	    WHEN -6 THEN 'ANALISA PINJAMAN DITOLAK AO'
        ELSE 'NO'
    END AS status_prakarsa,
    count (prakarsa.id) AS Total_Prakarsa,
    SUM (CAST(prakarsa_crs.loan_amount as FLOAT)) AS nominal_pinjaman
        FROM prakarsa 
        LEFT JOIN prakarsa_crs ON prakarsa_crs.prakarsa_id = prakarsa.id
        WHERE prakarsa.user_id IN (
            SELECT personal_number 
            FROM employees 
            WHERE branch_code NOT IN ('9999')
        )
    GROUP BY prakarsa.status
    ORDER BY prakarsa.status ASC;
`

const getPrakarsaByDate = `
    select
    CASE prakarsa.status
        WHEN 0 THEN 'PROSES INPUT PRESCREENING (0)'
        WHEN 1 THEN 'PROSES INPUT PRESCREENING (1)'
        WHEN 2 THEN 'MENUNGGU HASIL PRESCREENING'
        WHEN 3 THEN 'PRESCREENING DISETUJUI'
        WHEN 4 THEN 'PROCESS INPUT CRS'
        WHEN 5 THEN 'MENUNGGU PUTUSAN'
        WHEN 6 THEN 'PRAKARSA DISETUJUI, MINTA PERSETUJUAN NASABAH'
        WHEN 7 THEN 'PROSES AKAD KREDIT'
        WHEN 8 THEN 'PROSES PENCAIRAN'
        WHEN 9 THEN 'PENCAIRAN BERHASIL'
        WHEN -1 THEN 'PRESCREENING ERROR'
        WHEN -2 THEN 'PRESCREENING DITOLAK'
        WHEN -3 THEN 'PRAKARSA DITOLAK'
        WHEN -4 THEN 'PRAKARSA DITOLAK NASABAH'
        WHEN -5 THEN 'PRESCREENING DITOLAK AO'
	    WHEN -6 THEN 'ANALISA PINJAMAN DITOLAK AO'
        ELSE 'NO'
    END AS status_prakarsa,
    count (prakarsa.id) AS Total_Prakarsa,
    SUM (CAST(prakarsa_crs.loan_amount as FLOAT)) AS nominal_pinjaman
        FROM prakarsa 
        LEFT JOIN prakarsa_crs ON prakarsa_crs.prakarsa_id = prakarsa.id
        WHERE prakarsa.user_id IN (
            SELECT personal_number 
            FROM employees 
            WHERE branch_code NOT IN ('9999')
        )
    and prakarsa.updated_at >= '${currentDate} 00:00:00' and prakarsa.updated_at <= '${currentDate} 23:59:59'
    GROUP BY prakarsa.status
    ORDER BY prakarsa.status ASC;
`

const getAllPrakarsa = `
    SELECT 
        prakarsa.id AS prakarsa_id, prakarsa.created_at AS tanggal_prakarsa, prakarsa.ktp_number AS ktp_nasabah, prakarsa.name AS nama_nasabah, 
    CASE prakarsa.status
        WHEN 0 THEN 'PROSES INPUT PRESCREENING'
        WHEN 1 THEN 'PROSES INPUT PRESCREENING'
        WHEN 2 THEN 'MENUNGGU HASIL PRESCREENING'
        WHEN 3 THEN 'PRESCREENING DISETUJUI'
        WHEN 4 THEN 'PROCESS INPUT CRS'
        WHEN 5 THEN 'MENUGGU PUTUSAN'
        WHEN 6 THEN 'PRAKARSA DISETUJUI, MINTA PERSETUJUAN NASABAH'
        WHEN 7 THEN 'PROSES AKAD KREDIT'
        WHEN 8 THEN 'PROSES PENCAIRAN'
        WHEN 9 THEN 'PENCAIRAN BERHASIL'
        WHEN -1 THEN 'PRESCREENING ERROR'
        WHEN -2 THEN 'PRESCREENING DITOLAK'
        WHEN -3 THEN 'PRAKARSA DITOLAK'
        WHEN -4 THEN 'PRAKARSA DITOLAK NASABAH'
        ELSE 'NO'
    END AS status_prakarsa,
        employees.personal_number AS personal_number_ao, employees.fullname AS nama_ao, employees.branch_code AS kode_cabang,
    CASE employees.branch_code
        WHEN '0012' THEN 'Kantor Pusat'
        ELSE employees.organization_name
    END AS nama_cabang,
    employees.organization_name AS nama_organisasi,
    prakarsa_crs.financial_type AS tipe_pinjaman, prakarsa_crs.loan_amount AS nominal_pinjaman, prakarsa_crs.total_loan_repayment AS total_pengembalian_pinjaman,automation_facilities.facility_number, DATEDIFF (day,prakarsa.created_at,prakarsa.updated_at) as SLA, prakarsa.created_at 
    FROM prakarsa 
    INNER JOIN employees ON employees.personal_number = prakarsa.user_id
    LEFT JOIN prakarsa_crs ON prakarsa_crs.prakarsa_id = prakarsa.id
    LEFT JOIN automation_facilities ON automation_facilities.prakarsa_id = prakarsa.id
    WHERE prakarsa.user_id IN (
        SELECT personal_number 
        FROM employees 
    WHERE branch_code NOT IN ('9999')
    )
    ORDER BY prakarsa.name ASC;
`

const getPipeline = `
    SELECT
        partnerships.name as partner, partnerships.id as id_partner, partnerships.code,pc.branch_code,b.name as data_pipeline_cabang,	automation_facilities.facility_number as nomor_fasilitas,automation_loan_accounts.loan_account_number  as nomor_rekening_pinjaman,letter_numbers_sph.letter_number as nomor_akad, prakarsa.id as prakarsa_id,
    CASE pc.status
            WHEN 0 THEN 'Data Berhasil Diinput'
            WHEN 1 THEN 'Pinjaman Dalam Proses'
            WHEN 2 THEN 'Pinjaman Disetujui, Proses Persetujuan Mitra'
            WHEN 3 THEN 'Pinjaman Aktif'
            WHEN -2 THEN 'Pinjaman Belum Dapat Disetujui'
            else 'NO'
    end as Status_pipeline,
        prakarsa.created_at AS tanggal_prakarsa, pc.ktp_number AS ktp_nasabah, pc.name AS nama_nasabah, 
    CASE prakarsa.status
        WHEN 0 THEN 'PROSES INPUT PRESCREENING'
        WHEN 1 THEN 'PROSES INPUT PRESCREENING'
        WHEN 2 THEN 'MENUNGGU HASIL PRESCREENING'
        WHEN 3 THEN 'PRESCREENING DISETUJUI'
        WHEN 4 THEN 'PROCESS INPUT CRS'
        WHEN 5 THEN 'MENUNGGU PUTUSAN'
        WHEN 6 THEN 'PRAKARSA DISETUJUI, MINTA PERSETUJUAN NASABAH'
        WHEN 7 THEN 'PROSES AKAD KREDIT'
        WHEN 8 THEN 'PROSES PENCAIRAN'
        WHEN 9 THEN 'PENCAIRAN BERHASIL'
        WHEN -1 THEN 'PRESCREENING ERROR'
        WHEN -2 THEN 'PRESCREENING DITOLAK'
        WHEN -3 THEN 'PRAKARSA DITOLAK'
        WHEN -4 THEN 'PRAKARSA DITOLAK NASABAH'
        ELSE 'NO'
    END AS status_prakarsa,
        employees.personal_number AS personal_number_ao, employees.fullname AS nama_ao, 
    CASE 
        when automation_facilities.facility_number like '0012/0000203753/PK2/001' then '0010'
        when automation_facilities.facility_number like '0012/0000203794/PK2/001' then '1110'
        when automation_facilities.facility_number like '0012/0000203904/PK2/001' then '0010'
        ELSE employees.branch_code
    END AS kode_cabang,
    CASE 
        WHEN employees.branch_code = '0140' THEN 'CIREBON' --belum ada di tabel branch
        when automation_facilities.facility_number like '0012/0000203753/PK2/001' then 'KANTOR CABANG UTAMA'
        when automation_facilities.facility_number like '0012/0000203794/PK2/001' then 'MEDAN'
        when automation_facilities.facility_number like '0012/0000203904/PK2/001' then 'KANTOR CABANG UTAMA'
        ELSE branch.name 
    END AS nama_cabang,
    employees.organization_name AS nama_organisasi,
        prakarsa_crs.financial_type AS tipe_pinjaman, prakarsa_crs.loan_amount AS nominal_pinjaman, prakarsa_crs.total_loan_repayment AS total_pengembalian_pinjaman
    FROM potential_customers pc 
    left join prakarsa on prakarsa.id = pc.prakarsa_id 
    left join partnerships on partnerships.id = pc.partnership_id
    left JOIN employees ON employees.personal_number = prakarsa.user_id
    LEFT JOIN prakarsa_crs ON prakarsa_crs.prakarsa_id = prakarsa.id
    LEFT JOIN automation_facilities ON automation_facilities.prakarsa_id = prakarsa.id
    LEFT JOIN automation_loan_accounts ON automation_loan_accounts.prakarsa_id = prakarsa.id
    LEFT JOIN letter_numbers_sph ON letter_numbers_sph.prakarsa_id  = prakarsa.id
    left JOIN branch ON branch.code = employees.branch_code 
    left JOIN branch b ON b.code = pc.branch_code 
    ORDER BY prakarsa.id desc;
`

const getStatus78 = `
    SELECT	prakarsa.id AS prakarsa_id, prakarsa.created_at AS tanggal_prakarsa, prakarsa.ktp_number AS ktp_nasabah, automation_saving_accounts.saving_account_number, prakarsa.name AS nama_nasabah, 
    CASE prakarsa.status
        WHEN 0 THEN 'PROSES INPUT PRESCREENING'
        WHEN 1 THEN 'PROSES INPUT PRESCREENING'
        WHEN 2 THEN 'MENUNGGU HASIL PRESCREENING'
        WHEN 3 THEN 'PRESCREENING DISETUJUI'
        WHEN 4 THEN 'PROCESS INPUT CRS'
        WHEN 5 THEN 'MENUGGU PUTUSAN'
        WHEN 6 THEN 'PRAKARSA DISETUJUI, MINTA PERSETUJUAN NASABAH'
        WHEN 7 THEN 'PROSES AKAD KREDIT'
        WHEN 8 THEN 'PROSES PENCAIRAN'
        WHEN 9 THEN 'PENCAIRAN BERHASIL'
        WHEN -1 THEN 'PRESCREENING ERROR'
        WHEN -2 THEN 'PRESCREENING DITOLAK'
        WHEN -3 THEN 'PRAKARSA DITOLAK'
        WHEN -4 THEN 'PRAKARSA DITOLAK NASABAH'
        ELSE 'NO'
    END AS status_prakarsa,
        employees.personal_number AS personal_number_ao, employees.fullname AS nama_ao, employees.branch_code AS kode_cabang,
    CASE employees.branch_code
        WHEN '0012' THEN 'Kantor Pusat'
        ELSE employees.organization_name
    END AS nama_cabang,
    employees.organization_name AS nama_organisasi,
        prakarsa_crs.financial_type AS tipe_pinjaman, prakarsa_crs.loan_amount AS nominal_pinjaman, prakarsa_crs.total_loan_repayment AS total_pengembalian_pinjaman
    FROM prakarsa 
    INNER JOIN employees ON employees.personal_number = prakarsa.user_id
    LEFT JOIN prakarsa_crs ON prakarsa_crs.prakarsa_id = prakarsa.id
    left join automation_cif on automation_cif.prakarsa_id = prakarsa.id
    left join automation_saving_accounts on automation_saving_accounts.prakarsa_id = prakarsa.id
    WHERE prakarsa.user_id IN (
        SELECT personal_number 
        FROM employees 
        WHERE branch_code NOT IN ('9999')
    )
    and prakarsa.status in ('7','8')
    ORDER BY nama_cabang ASC;
`

const getAsuransi = (startDT, endDT) => {
    return `
        SELECT 
            prakarsa.created_at,
            prakarsa.ktp_number AS no_ktp, prakarsa.name AS nama_nasabah, 
            CONCAT(
                prakarsa.address, ', ', areas.village, ' ',  areas.district, ' ', areas.city, ' ', areas.province, ' ', prakarsa.postal_code
            ) AS alamat_nasabah,
            'MODAL KERJA' AS penggunaan_kredit,
            prakarsa_crs.loan_amount AS plafond_kredit, CAST(prakarsa_crs.loan_amount * 0.8 AS DECIMAL(16,2)) AS nilai_penjaminan,	economics_sub_sector.sub_sector_name AS jenis_usaha,
            prakarsa_crs.rate AS suku_bunga_tahunan, prakarsa_crs.loan_term AS jw_bulan,	letter_numbers_sph.signature_date AS tanggal_realisasi, 
            contract_logs.due_date AS tanggal_jatuh_tempo,
            'SKMA' AS jenis_agunan,
            (
                SELECT SUM(estimated_collateral_value) 
                FROM crs_collateral 
                WHERE prakarsa_id = prakarsa.id
            ) AS nilai_agunan,
            prakarsa_crs.total_employee AS tenaga_kerja,
            '0' AS jenis_terjamin,	prakarsa_crs.insurance_amount AS ijp,	automation_loan_accounts.loan_account_number AS loan_number,
            prakarsa_crs.insurance_type AS jenis_asuransi,	automation_facilities.facility_number 
        FROM prakarsa 
        INNER JOIN prakarsa_crs ON prakarsa_crs.prakarsa_id = prakarsa.id
        INNER JOIN areas ON areas.postal_code = prakarsa.postal_code 
            AND areas.district_code = prakarsa.district_code
            AND areas.village_code = prakarsa.village_code
        INNER JOIN business ON business.prakarsa_id = prakarsa.id
        LEFT JOIN economics_sub_sector ON economics_sub_sector.id = business.sub_economic_sector
        LEFT JOIN letter_numbers_sph ON letter_numbers_sph.prakarsa_id = prakarsa.id
        LEFT JOIN contract_logs ON contract_logs.prakarsa_id = prakarsa.id
        LEFT JOIN automation_cif ON automation_cif.prakarsa_id = prakarsa.id
        LEFT JOIN automation_facilities ON automation_facilities.prakarsa_id = prakarsa.id
        LEFT JOIN automation_loan_accounts ON automation_loan_accounts.prakarsa_id = prakarsa.id
        WHERE prakarsa.user_id IN (
            SELECT personal_number 
            FROM employees 
            WHERE branch_code NOT IN ('9999')
        )
        AND prakarsa.status = 9
        --AND (prakarsa_crs.insurance_type = 'Askrindo' OR prakarsa_crs.insurance_type IS NULL)
        and prakarsa.updated_at >= '${startDT}' and prakarsa.updated_at <= '${endDT}'
        ORDER BY prakarsa.name ASC;
    `
}

module.exports = {
  getPrakarsa, 
  getPrakarsaByDate, 
  getAllPrakarsa,
  getPipeline,
  getStatus78,
  getAsuransi
}
