const sqlConfig = {
  user: process.env.DB_USER_CORE,
  password: process.env.DB_PASSWORD_CORE,
  database: process.env.DB_NAME_CORE,
  server: process.env.DB_HOST_CORE,
  pool: {
    max: 10,
    min: 0,
    idleTimeoutMillis: 30000
  },
  options: {
    encrypt: false, // for azure
    trustServerCertificate: true // change to true for local dev / self-signed certs
  }
}

module.exports = sqlConfig