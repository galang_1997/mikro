const sqlConfigMikro = {
  user: process.env.DB_USER_MIKRO,
  password: process.env.DB_PASSWORD_MIKRO,
  database: process.env.DB_NAME_MIKRO,
  server: process.env.DB_HOST_MIKRO,
  pool: {
    max: 10,
    min: 0,
    idleTimeoutMillis: 30000
  },
  options: {
    encrypt: false, // for azure
    trustServerCertificate: true // change to true for local dev / self-signed certs
  }
}

module.exports = sqlConfigMikro