require('dotenv').config()
const logger = require("./logger/logger");
const TelegramBot = require('node-telegram-bot-api')
const ReportPerformaControllers = require('./controllers/ReportPerformaControllers')
const AllPrakarsaControllers = require('./controllers/AllPrakarsaControllers')
const PipelineControllers = require('./controllers/PipelineControllers')
const Status78Controllers = require('./controllers/Status78Controllers')
const AsuransiControllers = require('./controllers/AsuransiControllers')
const RealisasiPikroControllers = require('./controllers/RealisasiPikroControllers')
const DpkPikroControllers = require('./controllers/DpkPikroControllers')

const token = process.env.TOKEN_TELEGRAM

logger.info("[START BOT]");

const bot = new TelegramBot(token, {
  polling: true
});

bot.onText(/\/getsummary_performa/, async (msg) => {
  const chatId = msg.chat.id
  let reaportPerforma = await ReportPerformaControllers.getReaportPerforma()
  if (reaportPerforma) bot.sendMessage(chatId,reaportPerforma)
});

bot.onText(/\/getall_prakarsa/, async (msg) => {
  const chatId = msg.chat.id
  if (await AllPrakarsaControllers.getAllPrakarsa()) bot.sendDocument(chatId,'./Doc/ALL_PRAKARSA.xlsx')
});

bot.onText(/\/getpipeline/, async (msg) => {
  const chatId = msg.chat.id
  if (await PipelineControllers.getPipeline()) bot.sendDocument(chatId,'./Doc/PIPELINE.xlsx')
});

bot.onText(/\/getstatus78/, async (msg) => {
  const chatId = msg.chat.id
  if (await Status78Controllers.getStatus78()) bot.sendDocument(chatId,'./Doc/STATUS78.xlsx')
});

bot.onText(/\/getrealisasipikro/, async (msg) => {
  const chatId = msg.chat.id
  if (await RealisasiPikroControllers.getRealisasiPikro()) bot.sendDocument(chatId,'./Doc/REALISASI_PIKRO.xlsx')
});

bot.onText(/\/getdpkpikro/, async (msg) => {
  const chatId = msg.chat.id
  if (await DpkPikroControllers.getDpkPikro()) bot.sendDocument(chatId,'./Doc/DPK_PIKRO.xlsx')
});

bot.on('message', async (msg) => {
  const chatId = msg.chat.id;
  let message = msg.text.split('_')

  if (message[0] == '/getasuransi') {
    let month = message[1].slice(0,2)
    let year = message[1].slice(2,6)
    let lastDate = new Date(+year, +month, 0).getDate().toString();

    let startDT = `${year}-${month}-01 00:00:00`
    let endDT = `${year}-${month}-${lastDate} 23:59:59`

    if (await AsuransiControllers.getAsuransi(startDT, endDT)) bot.sendDocument(chatId,'./Doc/ASURANSI.xlsx')
  }
});

async function name(params) {
  console.log(await ReportPerformaControllers.getReaportPerforma());
}

name()