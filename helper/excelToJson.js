let parser = require('simple-excel-to-json')

let error
let data

function excelToJson() {
    try {
        data = parser.parseXls2Json('ANOMALY_CR_SCORE_SUB.xlsx'); 
    } catch (err) {
        error = err
    }

    return {data, error}
}

module.exports = excelToJson