let json2xls = require('json2xls')
const fs = require('fs')
let excelToJson = require('./excelToJson')

function toExcel(fileName, data, isAppend) {
    let result
    if (isAppend) {
        result = excelToJson()
        if(result.error) {
            if (result.error.errno != -4058) return
        }
        
        if (result.data) {
            if (result.data[0]) {
                result.data[0].forEach(res => {
                    data.push(res)
                });
            }
        }
    } 

    let xls = json2xls(data)
    fs.writeFileSync(`./Doc/${fileName}`, xls, 'binary')
}

module.exports = toExcel